# ADs made simplER.

* Create, manage and group ads
* Create and manage ad campaigns
* Explore ad statistics

Inspired by [Drupal 7 version of SimpleAds](https://www.drupal.org/project/simpleads) and alternative to [Doubleclick for Publishers (DFP)](https://www.drupal.org/project/dfp).
